﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="_About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <article id="content" style="min-height: 565px;">
        <div class="innerContent">
            <div class="pos">
                <div id="mainabout">
                    
                        <div class="images">
                            <asp:Repeater ID="AboutRepeater" runat="server">
                                <ItemTemplate>
                                    <div>
                                        <img src="About_Images/<%# Eval("MainImage") %>" height="567" />
                                        <div class="abouttxt">

                                            <pre id="abColor">
<%--                            <asp:Label Text='<%# Eval("Title") %>' runat="server" />--%>
                            <asp:Label Text='<%# Eval("Description") %>' runat="server" />
                          </pre>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>



                        </div>

                        <!-- the tabs -->

                        <div class="AboutTabs">
                            <ul>
                                <asp:Repeater ID="ItemRepeater" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <%--                                <a href="#"><%# Eval("Title") %></a>                                --%>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>


                        </div>


                    </div>
              
            </div>
        </div>

    </article>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>window.jQuery || document.write("<script src='assets/js/libs/jquery-1.11.0.min.js'>\x3C/script>")</script>
    <script type="text/javascript" src="assets/js/supersized.3.2.7.min.js"></script>
    <script src="assets/js/functions.js"></script>
    <script src="js/jquery.tools.min.js"></script>
    <script>
        $(function () {
            $(".AboutTabs").tabs(".images > div", { rotate: true, autoplay: true, interval: 800 }).slideshow();

        });
    </script>

</asp:Content>

