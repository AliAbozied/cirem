﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _About : System.Web.UI.Page
{
    AboutDAL aboutDAL = new AboutDAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        List<About> aboutList = aboutDAL.GetAllItems();

        
        this.AboutRepeater.DataSource = aboutList;
        this.AboutRepeater.DataBind();

        this.ItemRepeater.DataSource = aboutList;
        this.ItemRepeater.DataBind();

    }
}